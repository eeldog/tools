FROM alpine

RUN apk add -q --no-cache --no-progress \
		ghostscript \
		qpdf \
		exiftool \
		;

COPY init.sh css-unminify pdf-optimize /docker/

VOLUME  /work
WORKDIR /work

ENTRYPOINT [ "/docker/init.sh" ]
CMD []
