#! /bin/sh -

case "$1" in
css-unminify) ;;
pdf-optimize) ;;
*) exit 64    ;;
esac

command="/docker/$1"
shift;

exec "${command}" "$@";
exit 1;
